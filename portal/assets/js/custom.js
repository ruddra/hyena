$(document).ready(function () {
    $("button").click(function () {
        var query = $('.madeleine_search').val();

        $.ajax(
            {
                type: "POST",
                url: '/parse_url',
                data: {'use_url': query},
                success: function (result) {
                    $('#main-cont').html(result);
                }
            });


    });
    $('.madeleine_result').show();
});
