from handler import MainHandler, ParsingDataHandler

__author__ = 'arnabkumarshil'


routes = [
    (r"/", MainHandler),
    (r"/parse_url", ParsingDataHandler),
]