from parse import ContentParser

__author__ = 'arnabkumarshil'

import tornado.ioloop
import tornado.web


class MainHandler(tornado.web.RequestHandler):
    def get(self):
        self.render('index.html')


class ParsingDataHandler(tornado.web.RequestHandler):
    def post(self):
        use_url = self.get_argument('use_url')
        con_parse = ContentParser(use_url)
        con_parse.do_all()
        html_data = con_parse.make_html()
        return self.write(html_data)
