def set_header(header_value):
    return '{0}{1}{2}'.format('-'*20, header_value, '-'*20)
