import os
import tornado.ioloop
import tornado.web
import routes
import logging
import tornado.options
import options

from tools import set_header

app_settings = {
    'template_path': os.path.join(os.path.dirname(__file__), "portal/template"),
    'static_path': os.path.join(os.path.dirname(__file__), "portal/assets"),
}
application = tornado.web.Application(routes.routes, **app_settings)
tornado.options.define('port', options.server_settings['port'], int, 'The port the application should use. '
                                                                     'Overrides options_.configurations["port"]')


if __name__ == "__main__":
    logger = logging.getLogger(__name__)
    tornado.options.parse_command_line()
    # logger.setLevel(logging.DEBUG)
    logger.info(set_header(' Starting Marketing Tool HYENA '))
    logger.info(set_header(' Developed By Arnab Kumar Shil '))
    logger.info(set_header('Starting at {0}:{1}'.format(options.server_settings['address'],
                                                        options.server_settings['port'])))
    application.listen(tornado.options.options.port, xheaders=True)
    tornado.ioloop.IOLoop.current().start()

