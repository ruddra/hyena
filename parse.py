import requests
import re
import json
import datetime
from options import URL_MATCHERS, PHONE_NO_MATCHERS, HYPERLINK_MATCHERS, EMAIL_MATCHERS
from validators import validate_urls
import bs4


class ContentParser(object):
    _url = None
    data = dict()
    _html = None
    _soap = None

    def __init__(self, url):
        self._url = url

    def get_html(self):
        req = requests.get(self._url)
        self._html = req.text
        self._soap = bs4.BeautifulSoup(self._html, 'html.parser')

    def parse_urls_from_html(self):
        urls, hyperlinks = '', ''
        for ureg in URL_MATCHERS:
            urls += ', '.join(re.findall(ureg, self._html))

        # for hreg in HYPERLINK_MATCHERS:
        #     hyperlinks += ', '.join(re.findall(hreg, self._html))

        self.data['Urls'] = urls
        self.data['Hyperlinks'] = ', '.join([link.get('href') for link in self._soap.find_all('a')])

    def parse_emails(self):
        emails = ''
        for ereg in EMAIL_MATCHERS:
            emails += ', '.join(re.findall(ereg, self._html))

        self.data['Emails'] = emails

    def parse_text(self):
        self.data['Text'] = self._soap.get_text()

    def parse_phone_numbers(self):
        phones = ''
        for preg in PHONE_NO_MATCHERS:
            phones += ', '.join(re.findall(preg, self._html))
        self.data['PhoneNumbers'] = phones

    def do_all(self):
        try:
            if validate_urls(self._url):
                self.get_html()
                self.parse_emails()
                self.parse_phone_numbers()
                self.parse_urls_from_html()
                self.parse_text()
            else:
                print('Invalid URL')
        except Exception as e:
            print(e)

    def make_html(self):
        html_data = '<br/><h2>Base Url:\t\t{0}</h2><br/><hr/>'.format(self._url)
        for key, value in self.data.items():
            html_data += '<h3>{0}</h3><br/>'.format(key)
            html_data += '<p>{0}</p><br/>'.format(value)

        html_data += '<br/>'

        return html_data

