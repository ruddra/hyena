import re


def validate_urls(_url):
    rep = re.match(r'(https?://[^\s]+)', _url)
    return rep
